# *TerraFERMA* Benchmarks (repository deprecated)

**PLEASE VISIT [https://github.com/TerraFERMA/benchmarks](https://github.com/TerraFERMA/benchmarks)**

From July 2015 this repository of benchmarks for TerraFERMA, the *Transparent Finite Element Rapid Model Assembler*, has been deprecated.  To find
out more about the ongoing TerraFERMA project please visit our new website at
[http://terraferma.github.io/](http://terraferma.github.io) or go directly to the new benchmarks repository at
[https://github.com/TerraFERMA/benchmarks](https://github.com/TerraFERMA/benchmarks).

Many thanks for your interest in TerraFERMA!

